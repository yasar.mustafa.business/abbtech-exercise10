package org.example.task2;

public class InvalidInputException extends Exception{
    public InvalidInputException(String message) {
        super(message);
    }
}
