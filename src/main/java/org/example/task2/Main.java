package org.example.task2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        while(true){
            System.out.print("Give the value (temperature range: [15, 30]): ");
            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt();

            try {
                Temperature.checkTheRange(n);
            }
            catch (InvalidInputException exception){
                System.out.println(exception.getMessage());
            }
            System.out.println();
        }
    }
}
