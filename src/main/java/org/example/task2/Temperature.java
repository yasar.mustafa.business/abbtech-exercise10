package org.example.task2;

public class Temperature {

    public static final int MIN_RANGE = 15;
    public static final int MAX_RANGE = 30;

    static void checkTheRange(int value) throws InvalidInputException {
        if(value < MIN_RANGE || value > MAX_RANGE){
            throw new InvalidInputException("The given value is outside the range ... !");
        }
        System.out.println("Temperature is " + value + " Celsius.");
    }

}
