package org.example.task1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        while(true){
            System.out.print("Enter an integer value: ");
            Scanner scanner = new Scanner(System.in);
            String n = scanner.nextLine();

            try{
                Integer integer = Integer.parseInt(n);
                System.out.println("Square of the given number: " + (long)integer * (long)integer);
                break;
            }
            catch (NumberFormatException exception){
                System.out.println("Given value is not integer. Please provide an integer ...");
            }
        }

    }
}